import './App.css';
import {Component, useState} from "react";
import {nanoid} from 'nanoid';
import Task from "./components/Task/Task";
import AddTaskForm from "./components/AddTaskForm/AddTaskForm";


const App = () => {
  const [tasks, setTasks] = useState([
    {text: 'Buy milk', key: 1, id: 1, isDone: false},
    {text: 'Walk with dog', key: 2, id: 2, isDone: false},
    {text: 'Do homework', key: 3, id: 3, isDone: false},
  ]);

  const [currentTask, setCurrentTask] = useState('');

  const deleteTask = id => {
    setTasks(tasks.filter(p => p.id !== id));
  };

  const toggleTaskDone = id => {
      setTasks(tasks.map(task => {
          if(task.id === id) {
              task.isDone = !task.isDone;
          }
          return task;
      }));
  };

  const submitForm = (e) => {
    e.preventDefault();
    setTasks([...tasks, {text: currentTask, key: nanoid(), id: nanoid(), isDone: false}]);
    setCurrentTask('');
  }

  const changeText = (e) => {
      setCurrentTask(e.target.value);
  }

  const taskComponents = tasks.map(task => (
      <Task
          key={task.id}
          text={task.text}
          id={task.id}
          onDelete={() => deleteTask(task.id)}
          onToggleDone={() => toggleTaskDone(task.id)}
      >
      </Task>
  ));

  return (
    <div className="App container">
      <AddTaskForm
          currentTask={currentTask}
          onSubmit={e => submitForm(e)}
          onChange={e => changeText(e)}
      >
      </AddTaskForm>
      <div className="task-container">
        {taskComponents}
      </div>
    </div>
  );
}

export default App;
