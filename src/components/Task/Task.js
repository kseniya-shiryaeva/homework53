import React from 'react';
import './Task.css';
import removeImage from './images/trash-alt-solid.svg';

const Task = props => {
    return (
        <div className="task" id={props.id}>
            <input type="checkbox" className="toggle-done" onClick={props.onToggleDone} />
            <div className="task-body">{props.text}</div>
            <button type="button" className="remove-task" onClick={props.onDelete}> </button>
        </div>
    );
};

export default Task;