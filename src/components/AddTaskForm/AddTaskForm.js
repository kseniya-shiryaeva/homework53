import React, {useState} from 'react';
import './AddTaskForm.css';

const AddTaskForm = props => {
    return (
        <form id="add_task_form" onSubmit={props.onSubmit}>
            <input type="text" id="task_text" value={props.currentTask} onChange={props.onChange}/>
            <button type="submit">Add</button>
        </form>
    );
}

export default AddTaskForm;